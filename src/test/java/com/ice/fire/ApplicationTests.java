package com.ice.fire;

import com.ice.fire.apiobjects.Books;
import com.ice.fire.services.BooksServices;
import com.ice.fire.services.CharactersService;
import com.ice.fire.services.HousesService;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class ApplicationTests {

	@Autowired
	private BooksServices booksServices;
	@Autowired
	private CharactersService charactersService;
	@Autowired
	private HousesService housesService;


	@Autowired
	RestTemplate restTemplate;

	@Test
	public void booksService() throws Exception {
		assertThat(booksServices).isNotNull();
	}
	@Test
	public void characterService() throws Exception {
		assertThat(charactersService).isNotNull();
	}
	@Test
	public void houserService() throws Exception {
		assertThat(housesService).isNotNull();
	}

	@Test
	public void testMainAPIServer()
			throws ClientProtocolException, IOException {

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(new HttpGet("https://www.anapioficeandfire.com"));
		int statusCode = response.getStatusLine().getStatusCode();
		assertThat(statusCode).isEqualTo(200);
	}
	@Test
	public void testISBNGivenBookResource() throws Exception {
		Books bkArray = restTemplate.getForObject("https://www.anapioficeandfire.com/api/books/1"  , Books.class);
		System.out.println(bkArray.getIsbn());
		assertThat(bkArray.getIsbn().equalsIgnoreCase("978-0553103540"));
	}
	@Test
	public void testPoVGivenBookResource() throws Exception {
		Books bkArray = restTemplate.getForObject("https://www.anapioficeandfire.com/api/books/1"  , Books.class);
		System.out.println(bkArray.getIsbn());
		int povSize = bkArray.getPovCharacters().size();
		assertThat(povSize).isEqualTo(9);
	}

//	assertThat(list).contains("1");

	@Test
	public void testAListGivenBookResource() throws Exception {
		Books bkArray = restTemplate.getForObject("https://www.anapioficeandfire.com/api/books/1"  , Books.class);
		System.out.println(bkArray.getIsbn());
		List<String> list = bkArray.getAuthors();
		assertThat(list).contains("George R. R. Martin");
	}
}
