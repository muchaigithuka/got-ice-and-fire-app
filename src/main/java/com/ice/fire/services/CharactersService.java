package com.ice.fire.services;

import com.ice.fire.apiobjects.Books;
import com.ice.fire.apiobjects.Characters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sys")
public class CharactersService {
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/characters")
    public ModelAndView pullCharacters() {
        ModelAndView mv = new ModelAndView();
        List<Characters> characters = new ArrayList<Characters>();
        try {
            ResponseEntity<Characters[]> chArray = restTemplate.getForEntity("https://www.anapioficeandfire.com/api/characters", Characters[].class);
            Characters[] bkArrayBody = chArray.getBody();
            for (Characters bk : bkArrayBody) {
                characters.add(bk);
            }
            mv.setViewName("characters/Character");
            mv.addObject("charArr", characters);
            return mv;
        } catch (Exception e) {
            e.printStackTrace();
        }

        mv.setViewName("characters/Character");
        mv.addObject("character", "Something Went Wrong Try Again Later");
        return mv;
    }
    }
