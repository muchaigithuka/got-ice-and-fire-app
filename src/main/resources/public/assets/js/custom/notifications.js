   function success_msg(notification){
        PNotify.desktop.permission();
        (new PNotify({
            title: 'Success',
            text: notification,
            desktop: {
                desktop: true,
                addclass: 'bg-green',
            }
        })
        ).get().click(function(e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
        });
    }

 function error_msg(notification){
        PNotify.desktop.permission();
        (new PNotify({
            title: 'Error',
            type: 'danger',
            text: notification,
            desktop: {
                desktop: true,
            }
        })
        ).get().click(function(e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
        });
    }